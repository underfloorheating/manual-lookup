<?php

class Ufhs_Manuallookup_ManualinterfaceController extends Mage_Core_Controller_Front_Action
{
	/**
	* User Interface Controller
	*
	* @package Manual Lookup
	* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
	*/


	public function resultsAction()
	{
		$post = Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID)->getRequest()->getParams();

		// Generate list of prod IDs for next collection
		$collection = Mage::getModel('catalog/product')->getCollection();
		if(strlen($post['text']) > 0) {
			$searches = explode(" ", $post['text']);
			foreach ($searches as $search) {
				$collection->addAttributeToFilter(
					array(
						array('attribute'=> 'sku','like' => '%' . $search . '%'),
						array('attribute'=> 'name','like' => '%' . $search . '%'),
					)
				);
			}
		}
		if (isset($searches)) {
			$regArr = $this->_permutate($searches, '.*');
			$regexp = "(" . implode(')|(', $regArr) . ")";
		}
		if (isset($regexp)) {
			$regexp = rtrim($regexp,"|");
		}
		if(strlen($post['brand']) > 0) {
			$collection->addFieldToFilter('brand', $post['brand']);
		}
		if(strlen($post['prod_cat']) > 0) {
			$collection->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id', null, 'left')
			->addAttributeToFilter('category_id', $post['prod_cat']);
		}
		$collection->addAttributeToSelect('entity_id');
		$prods = [];
		foreach($collection->getData() as $item) {
			$prods[] = $item['entity_id'];
		}

		// If we haven't got any applicable products, set $prods to -1
		$lengthOfPost = array_sum(array_map(function($row){
			return strlen($row);
		},$post));

		if(empty($prods) || $lengthOfPost == 0) {
			$prods = -1;
		}



		// Using the prods array, set a collection containing all resources assigned to the products into a Magento regsitry
		$collection = Mage::getModel('manuallookup/resources')->getCollection();

		$keys = $conditions = [];
		// check if regexpstring varibale exist, build stuff
		// chekc if prods exist, buidl stuff
		if (isset($regexp)) {
			$keys [] = 'name';
			$conditions [] = ['regexp' => $regexp];
		}
		if ($prods) {
			$keys [] = 'prod_id';
			$conditions [] = ['in' => $prods];
		}
		if (!empty($keys)) {
			$collection = $collection->addFieldToFilter($keys, $conditions);
		} else {
			$collection = $collection->addFieldToFilter('prod_id', $prods);
		}

		$collection = $collection->distinct(true)
		->addFieldToSelect('filename')
		->addFieldToSelect('resource_type_id')
		->addFieldToSelect('name');
		Mage::register('resourceUICollection', $collection);

		$collection = Mage::getModel('manuallookup/videos')->getCollection();
		if (!empty($keys)) {
			$collection = $collection->addFieldToFilter($keys, $conditions);
		} else {
			$collection = $collection->addFieldToFilter('prod_id', $prods);
		}
		$collection = $collection->distinct(true)
		->addFieldToSelect('url')
		->addFieldToSelect('name');
		Mage::register('videoUICollection', $collection);

		Mage::register('postUICollection', $post);

		// Instantiate a new block using the output template and render it as the AJAX result
		$result = $this->getLayout()->createBlock('manuallookup/interfaceresults')->setTemplate('manuallookup/resource-interface.phtml')->toHtml();
		$counts = $this->getLayout()->createBlock('manuallookup/interfaceresults')->setTemplate('manuallookup/resource-counts.phtml')->toHtml();
		echo json_encode([
			'result' => $result,
			'count' => $counts
		]);
	}

	private function _permutate($words, $delimiter)
	{
		if ( count($words) <= 1 ) {
			$result = $words;
		} else {
			$result = array();
			for ( $i = 0; $i < count($words); ++$i ) {
				$firstword = $words[$i];
				$remainingwords = array();
				for ( $j = 0; $j < count($words); ++$j ) {
					if ( $i <> $j ) $remainingwords[] = $words[$j];
				}
				$combos = $this->_permutate($remainingwords, $delimiter);
				for ( $j = 0; $j < count($combos); ++$j ) {
					$result[] = $firstword . $delimiter . $combos[$j];
				}
			}
		}
		return $result;
	}
}