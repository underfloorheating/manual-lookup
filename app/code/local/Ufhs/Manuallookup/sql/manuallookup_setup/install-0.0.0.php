<?php
$installer = $this;
$installer->startSetup();
$installer->run("
	DROP TABLE IF EXISTS {$installer->getTable('manuallookup/resources')};
	CREATE TABLE {$installer->getTable('manuallookup/resources')} (
	`id` int(11) unsigned NOT NULL auto_increment,
	`prod_id` int(11) unsigned NOT NULL,
	`resource_type_id` int(11) unsigned NOT NULL,
	`filename` varchar(255) NOT NULL,
	`name` varchar(255) NOT NULL default '',
	PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

	DROP TABLE IF EXISTS {$installer->getTable('manuallookup/resourcestype')};
	CREATE TABLE {$installer->getTable('manuallookup/resourcestype')} (
	`id` int(11) unsigned NOT NULL auto_increment,
	`type` varchar(255) NOT NULL,
	PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

	DROP TABLE IF EXISTS {$installer->getTable('manuallookup/searchableattributes')};
	CREATE TABLE {$installer->getTable('manuallookup/searchableattributes')} (
	`id` int(11) unsigned NOT NULL auto_increment,
	`prod_id` int(11) unsigned NOT NULL,
	`brand` int(11) unsigned NOT NULL,
	`prod_cat` int(11) unsigned NOT NULL,
	`prod_type` int(11) unsigned NOT NULL,
	`prod_feature` int(11) unsigned NOT NULL,
	PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

	DROP TABLE IF EXISTS {$installer->getTable('manuallookup/productattributes')};
	CREATE TABLE {$installer->getTable('manuallookup/productattributes')} (
	`id` int(11) unsigned NOT NULL auto_increment,
	`attribute` varchar(255) NOT NULL,
	PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

	");
$installer->endSetup();