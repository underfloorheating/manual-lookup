<?php

class Ufhs_Manuallookup_Block_Resources extends Mage_Catalog_Block_Product_View_Tabs
{
	/**
	* Product Template Block
	*
	* @package Manual Lookup
	* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
	*/


	public function getDataList()
	{
		$store = Mage::app()->getStore()->getStoreId();
		$id = Mage::registry('current_product')->getId();
		$collection = Mage::getModel('manuallookup/resources')->getCollection()
		->addFieldToFilter(
			array('store', 'store'),
			array(
				array('like' => 0),
				array('like' => $store)
			)
		)
		->addFieldtoFilter('prod_id', $id);
		$return = [];
		foreach($collection->getData() as $item) {
			if($item['resource_type_id'] != 0) {
				$typeId = Mage::getModel('manuallookup/resourcestype')->load($item['resource_type_id'])->getType();
				$return[] = $item;
			}
		}
		$return = $this->_orderResources($return);
		return $return;
	}

	public function getVideoList()
	{
		$store = Mage::app()->getStore()->getStoreId();
		$id = Mage::registry('current_product')->getId();
		$collection = Mage::getModel('manuallookup/videos')->getCollection()
		->addFieldToFilter(
			array('store', 'store'),
			array(
				array('like' => 0),
				array('like' => $store)
			)
		)
		->addFieldToFilter('disabled', 0)
		->addFieldtoFilter('prod_id', $id);
		$return = [];
		foreach($collection->getData() as $item) {
			$return[] = $item;
		}
		$return = $this->_orderResources($return);
		return $return;
	}

	private function _orderResources($resources)
	{
		uasort($resources, function($a, $b){
			if ($a['order'] == $b['order']) {
				return 0;
			}
			return ($a['order'] < $b['order']) ? -1 : 1;
		});
		return $resources;
	}

	public function getProdId()
	{
		return Mage::registry('current_product')->getId();
	}

	public function showDefaultText()
	{
		return false;
	}
}