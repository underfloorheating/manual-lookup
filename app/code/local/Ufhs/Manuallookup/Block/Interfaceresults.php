<?php

class Ufhs_Manuallookup_Block_Interfaceresults extends Mage_Core_Block_Template
{
	/**
	* User Interface Results Block
	*
	* @package Manual Lookup
	* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
	*/


	public function getDataList()
	{
		$collection = Mage::registry('resourceUICollection');
		$return = [];
		foreach($collection->getData() as $item)
		{
			if($item['resource_type_id'] != 0)
			{
				$typeId = Mage::getModel('manuallookup/resourcestype')->load($item['resource_type_id'])->getType();
				$return[$typeId][] = $item;
			}
		}
		return $return;
	}

	public function getDataCounts()
	{
		$result = [];
		$data = $this->getDataList();
		foreach ($data as $key => $value) {
			$result[$key] = count($value);
		}
		return $result;
	}

	public function getSearch()
	{
		$post = Mage::registry('postUICollection');
		$return = [];
		if (!empty($post['text'])) {
			$return['text'] = $post['text'];
		}
		if (!empty($post['brand'])) {
			$return['brand'] = Mage::getModel('catalog/product')->getResource()->getAttribute('brand')->getSource()->getOptionText($post['brand']);
		}
		if (!empty($post['prod_cat'])) {
			$return['category'] = Mage::getModel('catalog/category')->load($post['prod_cat'])->getName();
		}
		return $return;
	}

	public function getTotalCount()
	{
		$count = 0;
		$data = $this->getDataList();
		foreach ($data as $value) {
			$count += count($value);
		}
		return $count;
	}

	public function showDefaultText()
	{
		return true;
	}

	public function getVideoList()
	{
		return Mage::registry('videoUICollection')->getData();
	}
}