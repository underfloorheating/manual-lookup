<?php
class Ufhs_Manuallookup_Block_Adminhtml_Viewresources_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('manuallookupViewresourcesGrid');
		$this->setDefaultSort('id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('manuallookup/resources')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('id', array(
			'header' => Mage::helper('manuallookup')->__('ID'),
			'align' => 'left',
			'width' => '10px',
			'index' => 'id'
			));
		$this->addColumn('prodid', array(
			'header' => Mage::helper('manuallookup')->__('Product SKU'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'prod_id',
			'renderer' => 'Ufhs_Manuallookup_Block_Adminhtml_Renderers_Productsku'
			));
		$this->addColumn('resourcetypeid', array(
			'header' => Mage::helper('manuallookup')->__('Type'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'resource_type_id',
			'renderer' => 'Ufhs_Manuallookup_Block_Adminhtml_Renderers_Resourcetype',
			'filter' => 'adminhtml/widget_grid_column_filter_select',
			'sortable' => true,
			'options' => $this->_getResourceTypes()
			));
		$this->addColumn('name', array(
			'header' => Mage::helper('manuallookup')->__('Name'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'name'
			));
		$this->addColumn('filename', array(
			'header' => Mage::helper('manuallookup')->__('Filename'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'filename'
			));

		$object = new Varien_Object(array('grid_block' => $this));
		Mage::dispatchEvent("manuallookup_block_adminhtml_viewresources_grid_preparecolumns", array("data" => $object));
		return parent::_prepareColumns();
	}

	public function getEmptyText()
	{
		return $this->__('No resources have been assigned to products yet.');
	}

	public function getRowUrl($row)
	{
		return $this->getUrl('*/catalog_product/edit', array('id' => $row->getData()['prod_id'], 'active_tab' => 'Resources'));
	}

	private function _getResourceTypes()
	{
		$returnArray = [];
		$collection = Mage::getModel('manuallookup/resources')->getCollection();
		foreach($collection as $item)
		{
			$returnArray[$item->getResourceTypeId()] = Mage::getModel('manuallookup/resourcestype')->load($item->getResourceTypeId())->getType();
		}
		return $returnArray;
	}
}