<?php
class Ufhs_Manuallookup_Block_Adminhtml_Settype_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('manuallookupSettypeGrid');
		$this->setDefaultSort('id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('manuallookup/resourcestype')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('id', array(
			'header' => Mage::helper('manuallookup')->__('ID'),
			'align' => 'left',
			'width' => '10px',
			'index' => 'id'
			));
		$this->addColumn('type', array(
			'header' => Mage::helper('manuallookup')->__('Type'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'type'
			));
		$this->addColumn('actions', array(
			'header'    => Mage::helper('manuallookup')->__('Action'),
			'width'     => '50px',
			'type'      => 'action',
			'getter'     => 'getId',
			'actions'   => array(
				array(
					'caption' => Mage::helper('manuallookup')->__('Delete'),
					'url'     => array(
						'base'=>'*/manuallookup/deletetype'
						),
					'field'   => 'id'
					)
				),
			'filter'    => false,
			'sortable'  => false,
			'index'     => 'stores',
			));

		$object = new Varien_Object(array('grid_block' => $this));
		Mage::dispatchEvent("manuallookup_block_adminhtml_settype_grid_preparecolumns", array("data" => $object));
		return parent::_prepareColumns();
	}

	public function getRowUrl($row)
	{
		return $this->getUrl('*/*/edittype', array('id' => $row->getData()['id']));
	}

	public function getEmptyText()
	{
		return $this->__('No resource types have been created yet.');
	}
}