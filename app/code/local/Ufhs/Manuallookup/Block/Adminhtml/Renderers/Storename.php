<?php
class Ufhs_Manuallookup_Block_Adminhtml_Renderers_Storename extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		$value =  $row->getData($this->getColumn()->getIndex());
		if ($value == '0') {
			return 'All Stores';
		}
		$store = Mage::getModel('core/store')->load($value);
		return $store->getName();
	}
}