<?php
class Ufhs_Manuallookup_Block_Adminhtml_Renderers_Linkyoutube extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		$value =  $row->getData($this->getColumn()->getIndex());
		return '<a href="https://www.youtube.com/watch?v=' . $value . '" target="_blank">' . $value . "</a>";
	}
}