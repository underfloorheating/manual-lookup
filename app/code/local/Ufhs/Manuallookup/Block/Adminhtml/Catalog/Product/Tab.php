<?php
class Ufhs_Manuallookup_Block_Adminhtml_Catalog_Product_Tab extends Mage_Adminhtml_Block_Template
implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    /**
     * Set the template for the block
     *
     */
    public function _construct()
    {
    	parent::_construct();
    	$this->setTemplate('manuallookup/catalog/product/tab.phtml');
    }

    /**
     * Retrieve the label used for the tab relating to this block
     *
     * @return string
     */
    public function getTabLabel()
    {
    	return $this->__('Resources');
    }

     /**
     * Retrieve the title used by this tab
     *
     * @return string
     */
     public function getTabTitle()
     {
     	return $this->__('Resources');
     }

    /**
     * Determines whether to display the tab
     * Add logic here to decide whether you want the tab to display
     *
     * @return bool
     */
    public function canShowTab()
    {
    	return true;
    }

    /**
     * Stops the tab being hidden
     *
     * @return bool
     */
    public function isHidden()
    {
    	return false;
    }

    public function getResourceTypes()
    {
    	$collection = Mage::getModel('manuallookup/resourcestype')->getCollection();
    	$return = '';
    	foreach($collection->getData() as $item)
    	{
    		$return .= '<option value="' . $item['id'] . '">' . $item['type'] . '</option>';
    	}
    	return $return;
    }

    public function getUploadUrl()
    {
    	return $this->getUrl('*/manuallookup/addresource', array('prodid' => $this->getRequest()->getParam('id')));
    }

    public function getVideoUrl()
    {
    	return $this->getUrl('*/manuallookup/addvideo', array('prodid' => $this->getRequest()->getParam('id')));
    }

    public function getLinkUrl()
    {
    	return $this->getUrl('*/manuallookup/linkresource', array('prodid' => $this->getRequest()->getParam('id')));
    }

    public function getExistingResources()
    {
    	$brand = Mage::getModel('catalog/product')->load($this->getRequest()->getParam('id'))->getBrand();
    	$collection = Mage::getModel('manuallookup/resources')->getCollection()->addFieldToSelect('prod_id');
    	$prods = [];

    	foreach ($collection as $item) {
    		$prods[] = $item['prod_id'];
    	}

    	if (empty($prods)) {
    		return false;
    	}

    	$products = Mage::getModel('catalog/product')
    	->getCollection()
    	->addAttributeToFilter('entity_id', $prods)
    	->addAttributeToFilter('brand', $brand);

    	$prods = [];
    	foreach ($products as $item) {
    		$prods[] = $item['entity_id'];
    	}

    	if (empty($prods)) {
    		return false;
    	}

    	$resources = Mage::getModel('manuallookup/resources')->getCollection()
    	->addFieldToFilter('prod_id', $prods)
    	->getData();

    	usort($resources, function ($a, $b) {
    		if ($a['name'] == $b['name']) {
    			return 0;
    		}
    		return ($a['name'] < $b['name']) ? -1 : 1;
    	});


    	$check = $newResources = [];
    	foreach ($resources as $item) {
    		if (!isset($check[$item['filename'] . $item['name']])) {
    			$check[$item['filename'] . $item['name']] = $item['id'];
    			$newResources[$item['id']] = $item['name'];
    		}
    	}

    	foreach ($newResources as $key => $item) {
    		echo '<option value="' . $key . '">' . $item . '</option>';
    	}
    }

}