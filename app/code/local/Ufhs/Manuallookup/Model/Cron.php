<?php

class Ufhs_Manuallookup_Model_Cron
{
	public function queryVideos()
	{
		$collection = Mage::getModel('manuallookup/videos')
		->getCollection()
		->addFieldToFilter('disabled', 0);
		foreach ($collection as $video) {
			if (!Ufhs_Manuallookup_Helper_Data::checkVideo($video['url'])) {
				$this->_disableVideo($video['id']);
				$this->_notifyVideo($video['prod_id'], $video['url']);
			}
		}
		return $this;
	}

	private function _notifyVideo($prod, $video)
	{
		$body = 'Product #' . $prod . "'s  video (" . $video . ') is no longer available.';

		$mail = Mage::getModel('core/email');
		$mail->setToName(Mage::getStoreConfig('trans_email/ident_manuallookup_dev/name'));
		$mail->setToEmail(Mage::getStoreConfig('trans_email/ident_manuallookup_dev/email'));
		$mail->setBody($body);
		$mail->setSubject('YouTube Video Is No Longer Available');
		$mail->setFromEmail('no-reply@theunderfloorheatingstore.com');
		$mail->setFromName('UFHS Manuallookup');
		$mail->setType('text');
		try {
			if(!$mail->send()) {
				throw new Expection('Unable to cron email.');
			}
		}
		catch (Exception $e) {
			throw new Expection($e->getMessages());
		}
	}

	private function _disableVideo($id)
	{
		Mage::getModel('manuallookup/videos')->load($id)->setDisabled(1)->save();
	}
}