<?php

class Ufhs_Manuallookup_Model_Observer
{
	/**
	 * Flag to stop observer executing more than once
	 *
	 * @var static bool
	 */
	static protected $_singletonFlag = false;

	/**
	 * This method will run when the product is saved from the Magento Admin
	 * Use this function to update the product model, process the
	 * data or anything you like
	 *
	 * @param Varien_Event_Observer $observer
	 */
	public function saveProductTabData(Varien_Event_Observer $observer)
	{
		if (!self::$_singletonFlag) {
			self::$_singletonFlag = true;

			try {
				if (!empty(Mage::app()->getRequest()->getParam('manuallookupVideoOrder'))) {
					foreach (Mage::app()->getRequest()->getParam('manuallookupVideoOrder') as $id => $value) {
						Mage::getModel('manuallookup/videos')->load($id)->setOrder($value)->save();
					}
				}

				if (!empty(Mage::app()->getRequest()->getParam('manuallookupResourceOrder'))) {
					foreach (Mage::app()->getRequest()->getParam('manuallookupResourceOrder') as $id => $value) {
						Mage::getModel('manuallookup/resources')->load($id)->setOrder($value)->save();
					}
				}
			}
			catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
	}
}
